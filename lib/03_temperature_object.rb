class Temperature

  def initialize(options)
    if options[:f]
      self.fahrenheit = options[:f] #references line 20
    else
      self.celsius = options[:c] #references line 25
    end
  end

  #class methods that do the conversion
  def self.ftoc(temp)
    (temp - 32) * (5 / 9.0)
  end
  def self.ctof(temp)
    (temp * 9 / 5.0) + 32
  end

  #setter methods that change temp to celsius no matter what
  def fahrenheit=(temp)
    @celsius = self.class.ftoc(temp)
    #self.class == Temperature (the class). allows access to ftoc
  end

  def celsius=(temp)
    @celsius = temp
  end
  #end of setter methods. need both cuz of what given to opts hash
  def in_fahrenheit
    self.class.ctof(@celsius)
  end

  def in_celsius
    @celsius
  end

  #factory method time woo
  def self.from_fahrenheit(temp)
    self.new(f: temp)
  end
  def self.from_celsius(temp)
    self.new(c: temp)
  end
end
  #end of factory method time boo
class Celsius < Temperature
  def initialize(temp)
    self.celsius = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    self.fahrenheit = temp
  end
end
