class Timer
  attr_accessor :seconds

  def initialize(seconds = 0)
    @seconds = seconds
  end

  def time_string
    hours = 0
    minutes = 0
    if @seconds >= 3600
      hours = @seconds / 3600
      @seconds = @seconds - (hours * 3600)
    end
    if @seconds >= 60
      minutes = @seconds / 60
      @seconds = @seconds - (minutes * 60)
    end
    time_str = "#{padder(hours)}:#{padder(minutes)}:#{padder(@seconds)}"
  end

  def padder(num)
    if num.to_s.length == 1
      num = "0" + num.to_s
    else
      num.to_s
    end
    num
  end

end
