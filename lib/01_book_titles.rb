class Book
  attr_accessor :title

  def title=(the_title)
    res = []
    bad_words = ["and", "a", "but", "or", "an", "of", "in", "the"]
    res_title = the_title.split.map.with_index do |word, idx|
      if word == "i"
        word = "I"
      end
      if idx == 0
        word = word.capitalize
      end
      if the_title.split[-1] == word
        word = word.capitalize
      end
      word = word.capitalize unless bad_words.include?(word)
      word
    end
    @title = res_title.join(" ")
  end
end
