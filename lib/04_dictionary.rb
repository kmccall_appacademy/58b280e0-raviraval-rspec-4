class Dictionary
  def initialize
    @d = {}
  end

  def entries
    @d
  end

  def add(new_entries)
    if new_entries.is_a?(String)
      @d[new_entries] = nil
    elsif new_entries.is_a?(Hash)
      @d.merge!(new_entries)
    end
    @d.sort_by { |k| k }
  end

  def include?(k)
    @d.has_key?(k)
  end

  def keywords
    @d.keys.sort
  end

  def find(k)
    @d.select { |key, v| key[0...k.length] == k }
  end

  def printable
    printed = @d.sort.map do |k, v|
      "[#{k}] \"#{v}\""
    end
    printed.join("\n")
  end
end
